package com.hnevc.androidui5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button mBtnCompute;
    TextView mTvMessage;//显示message的文本框
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        initEvent();
    }
    //申明接口，这个接口将会将message的信息显示到Activity
    interface OnComputed{
        void showMessage(String message);
    }
    private void initEvent() {
        //绑定事件
        mBtnCompute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //显示对话框
                MyDialog dialog = new MyDialog(MainActivity.this, new OnComputed() {
                    @Override
                    public void showMessage(String message) {
                        mTvMessage.setText(message);
                    }
                });
                dialog.show();
            }
        });
    }


    private void initView() {
        mBtnCompute = findViewById(R.id.btn_button1);//获取按钮
        mTvMessage = findViewById(R.id.tv_message);//获取文本框tv_message

    }
}
