package com.hnevc.androidui5;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Administrator on 2018/6/12.
 */

public class MyDialog extends Dialog {
    Context context;//当前对话框所在的上下文环境

    EditText mEtHeight;//输入身高的EditText
    EditText mEtWeight;
    Button mBtnCompute;
    Button mBtnCancel;

    MainActivity.OnComputed onComputed ;

    public MyDialog(@NonNull Context context,MainActivity.OnComputed onComputed) {
        super(context);
        this.context = context; //保存上下文到context
        this.onComputed = onComputed;//保存回调对象

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_dialog);

        initView();
        initEvent();
    }

    private void initEvent() {
        //绑定event

        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mBtnCompute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //计算
                String heights = mEtHeight.getText().toString();
                int height = Integer.parseInt(heights);

                String weights = mEtWeight.getText().toString();
                int weight = Integer.parseInt(weights);

                int standardWeight = (int)((height -80) * 0.7);//计算标准体重

                String message="";
                if (standardWeight > weight){
                    message = "偏瘦，多吃点！";
                }else if (standardWeight == weight){
                    message = "真棒！标准体重！";
                }else {
                    message = "偏胖，少吃点";
                }
                Toast.makeText(context,message, Toast.LENGTH_SHORT).show();
                //回调
                onComputed.showMessage(message);

            }
        });
    }

    private void initView() {
        mEtHeight = findViewById(R.id.et_height);
        mEtWeight = findViewById(R.id.et_weight);
        mBtnCompute = findViewById(R.id.btn_compute);
        mBtnCancel = findViewById(R.id.btn_cancel);

    }
}
